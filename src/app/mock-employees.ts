import { Employee } from './employee';

export const EMPLOYEES: Employee[] = [
  {name : "Astha", age: 23, address: "clock tower"},
  {name : "Chhavi", age: 22, address: "premnagar"}
];
