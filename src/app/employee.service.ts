import {Injectable} from '@angular/core';
import {Employee} from './employee';
import {EMPLOYEES} from './mock-employees';

/*import { Observable, of } from 'rxjs';*/
@Injectable({
  providedIn: 'root',
})
export class EmployeeService {
  employee = EMPLOYEES;

  constructor() {
  }

  getEmployee(): Employee[] {
    return this.employee;
  }

  addEmployees(add) {
    this.employee.push(add);
  }
  delEmployees(index) {
    this.employee.splice(index, 1);
  }


}
