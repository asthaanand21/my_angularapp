import {Component} from '@angular/core';
import {EmployeeService} from './employee.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Angular CRUD operation with array!!';
  employees;
  add: any = {};
  update: any = {};

  constructor(private employeeService: EmployeeService) {
  }

  getEmployees(): void {
    this.employees = this.employeeService.getEmployee();
    console.log('--------------->>>>', this.employees);
  }

  ngOnInit() {
    this.getEmployees();
  }

  addEmployee() {
    this.employeeService.addEmployees(this.add);
    this.add={};
  }

  deleteEmployee(i) {
    this.employeeService.delEmployees(i);
  }

}


